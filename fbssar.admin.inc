<?php

/**
 * @file
 *   Administrative settings for Facebook-style Statuses Auto-Respond.
 */

/**
 * Administrative settings form.
 */
function fbssar_settings($form_state = array()) {
  //drupal_add_css();
  $form['#cache'] = TRUE;
  //Form elements between ['opendiv'] and ['closediv'] will be refreshed via AHAH on form submission.
  $form['opendiv'] = array(
    '#value' => '<div id="fbssar_replace">',
  );
  $form['fbssar_person'] = array(
    '#type' => 'textfield',
    '#title' => t('User who will post auto-responses'),
    '#autocomplete_path' => 'user/autocomplete',
    '#default_value' => variable_get('fbssar_person', ''),
  );
  $form['table_init'] = array(
    '#value' => '<table><tr><td rowspan="2">',
  );
  $form['initial'] = array(
    '#type' => 'textarea',
    '#title' => t('Initial message'),
    '#rows' => 3,
  );
  $form['table_mid'] = array(
    '#value' => '</td><td rowspan="2">',
  );
  $form['response'] = array(
    '#type' => 'textarea',
    '#title' => t('Auto-response'),
    '#rows' => 3,
  );
  $form['table_mid_2'] = array(
    '#value' => '</td><td>',
  );
  $form['keep_orig'] = array(
    '#type' => 'checkbox',
    '#title' => t('Save initial status'),
    '#default_value' => 1,
  );
  $form['table_mid_3'] = array(
    '#value' => '</td></tr><tr><td>',
  );
  $form['other'] = array(
    '#type' => 'checkbox',
    '#title' => t("Apply to status messages on other users' profiles"),
  );
  $form['table_final'] = array(
    '#value' => '</td></tr></table>',
  );
  $form['save'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#ahah' => array(
      'path' => 'fbssar/js',
      'wrapper' => 'fbssar_replace',
      'effect' => 'fade',
      'method' => 'replace',
    ),
  );
  $form['existing'] = array(
    '#value' => fbssar_existing(),
  );
  //Form elements between ['opendiv'] and ['closediv'] will be refreshed via AHAH on form submission.
  $form['closediv'] = array(
    '#value' => '</div>',
  );
  return $form;
}

/**
 * Validation handler for the administrative settings.
 */
function fbssar_settings_validate($form, &$form_state) {
  $v = $form_state['values'];
  if($v['initial'] && !$v['response']) {
    form_set_error('response', t('To save an auto-responder, you must specify a response.'));
  }
  if ($v['fbssar_person']) {
    $account = user_load(array('name' => $v['fbssar_person']));
    if (!$account->uid) {
      form_set_error('fbssar_person', t('The name you specified for the auto-response user is invalid.'));
    }
  }
}

/**
 * Submit handler for the administrative settings.
 */
function fbssar_settings_submit($form, &$form_state) {
  $v = $form_state['values'];
  variable_set('fbssar_person', $v['fbssar_person']);
  if (trim($v['response'])) {
    fbssar_save($v['initial'], $v['response'], $v['keep_orig'], $v['other']);
  }
  $form_state['rebuild'] = TRUE;
  drupal_set_message(t('The auto-respond configuration has been saved.'));
}

/**
 * Saves the auto-responder via AHAH.
 */
function fbssar_save_js() {
  $form_state = array('storage' => NULL, 'submitted' => FALSE);
  $form_build_id = $_POST['form_build_id'];
  $form = form_get_cache($form_build_id, $form_state);
  $form_state['post'] = $form['#post'] = $_POST;
  $form['#programmed'] = $form['#redirect'] = FALSE;
  $args = $form['#parameters'];
  //This happens if someone goes directly to the JS processing page.
  if (!is_array($args) && !$args) {
    drupal_goto('admin/settings/facebook_status/autorespond');
    watchdog('fbssar', 'Someone tried to access the JavaScript processing page for Facebook-style Statuses Auto-Respond directly.', array(), WATCHDOG_DEBUG);
    return;
  }
  $form_id = array_shift($args);
  drupal_process_form($form_id, $form, $form_state);
  $form = drupal_rebuild_form($form_id, $form_state, $args, $form_build_id);
  //Get HTML for the replacement form. Only these elements will be AHAH-refreshed.
  $new_form['fbssar_person'] = $form['fbssar_person'];
  $new_form['table_init']  = $form['table_init'];
  $new_form['initial']     = $form['initial'];
  $new_form['table_mid']   = $form['table_mid'];
  $new_form['response']    = $form['response'];
  $new_form['table_mid_2'] = $form['table_mid_2'];
  $new_form['keep_orig']   = $form['keep_orig'];
  $new_form['table_mid_3'] = $form['table_mid_3'];
  $new_form['other']       = $form['other'];
  $new_form['table_final'] = $form['table_final'];
  $new_form['save']        = $form['save'];
  $new_form['existing']    = $form['existing'];
  //If the $form['save']['#ahah']['wrapper'] div was found in a #prefix or #suffix of a form element that we re-rendered here,
  //then we would have to unset() it to prevent duplicate wrappers. However, we have a somewhat unique implementation in which the wrappers
  //are actually their own elements, so this is not an issue.
  $output = theme('status_messages') . drupal_render($new_form);
  //Return the results.
  drupal_json(array('status' => TRUE, 'data' => $output));
}

/**
 * Edit auto-responder.
 */
function fbssar_edit($form_state, $ar) {
  $form = fbssar_settings();
  $form['initial']['#default_value'] = $ar->initial;
  $form['response']['#default_value'] = $ar->response;
  $form['keep_orig']['#default_value'] = $ar->keep_orig;
  $form['other']['#default_value'] = $ar->other;
  $form['#arid'] = $ar->arid;
  unset($form['fbssar_person']);
  unset($form['existing']);
  unset($form['save']['#ahah']);
  $form['cancel'] = array(
    '#type' => 'submit',
    '#value' => t('Cancel'),
    '#submit' => array('fbssar_cancel'),
  );
  return $form;
}

/**
 * Submit handler for editing an auto-responder.
 */
function fbssar_edit_submit($form, &$form_state) {
  $v = $form_state['values'];
  fbssar_save($v['initial'], $v['response'], $v['keep_orig'], $v['other'], $form['#arid']);
  drupal_set_message(t('Auto-response succesfully updated.'));
  $form_state['redirect'] = 'admin/settings/facebook_status/autorespond';
}

/**
 * Delete auto-responder.
 */
function fbssar_delete($form_state, $ar) {
  $form['message'] = array(
    '#value' => '<p>'. t('Are you sure you want to delete this auto-responder?') .'</p>',
  );
  $form['ar'] = array(
    '#value' => '<table><tr><th>'. t('Initial message') .'</th><th>'. t('Response')
      .'</th></tr><tr><td>'. check_plain($ar->initial) .'</td><td>'. check_plain($ar->response) .'</td></tr></table>',
  );
  $form['arid'] = array(
    '#type' => 'value',
    '#value' => $ar->arid,
  );
  $form['confirm'] = array(
    '#type' => 'submit',
    '#value' => t('Confirm'),
    '#submit' => array('fbssar_delete_submit'),
  );
  $form['cancel'] = array(
    '#type' => 'submit',
    '#value' => t('Cancel'),
    '#submit' => array('fbssar_cancel'),
  );
  return $form;
}

/**
 * Submit handler for deleting an auto-responder.
 */
function fbssar_delete_submit($form, &$form_state) {
  fbssar_delete_ar($form_state['values']['arid']);
  drupal_set_message(t('Auto-response deleted.'));
  $form_state['redirect'] = 'admin/settings/facebook_status/autorespond';
}

/**
 * Cancels editing or deletion.
 */
function fbssar_cancel($form, &$form_state) {
  $form_state['redirect'] = 'admin/settings/facebook_status/autorespond';
}

/**
 * Builds a list of existing auto-responders.
 */
function fbssar_existing() {
  $headers = array(t('Initial message'), t('Response'), t('Keep original'), t('Include cross-posts'), t('Operations'));
  $rows = array();
  $result = pager_query("SELECT * FROM {fbssar}");
  while ($row = db_fetch_object($result)) {
    $rows[] = array(
      check_plain($row->initial),
      check_plain($row->response),
      $row->keep_orig ? t('Yes') : t('No'),
      $row->other ? t('Yes') : t('No'),
      l('Edit', 'admin/settings/facebook_status/autorespond/'. $row->arid .'/edit') .' '.
        l('Delete', 'admin/settings/facebook_status/autorespond/'. $row->arid .'/delete'),
    );
  }
  return theme('table', $headers, $rows) . theme('pager');
}